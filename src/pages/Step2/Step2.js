import React, { Component } from 'react';
import { StyleSheet, Keyboard, Text, View, ActivityIndicator, ImageBackground, KeyboardAvoidingView } from 'react-native';
import { connect } from "react-redux";
import PropTypes from "prop-types";

import Notification from "../../components/Notification/Notification";
import Countdown from "../../components/Countdown/Countdown";
import Modal from "../../components/Modal/Modal";
import Input from "../../components/Input/Input";
import { loginUser } from "../../store/actions/authActions";
import Header from "../../components/Header/Header";
import Footer from "../../components/Footer/Footer";
import Button from "../../components/Button/Button";
import BgImage from "../../assets/images/bg.png";

class Step2 extends Component {

  // Reset header navigation
  static navigationOptions = {
    header: null
  };

  state = {
    isLoading: false,
    isAuthenticated: false,
    isModal: false,
    email: 'react@native.facebook.com',
    validPassword: 'pistol',
    password: '',
    disabled: 'disabled',
    isShowKeyboard: false,
    smsRequestDisabled: '',
    smsRequestText: "Не приходить SMS",
    count: 0,
    running: false,
    errors: {},
    isValidInput: true
  }

  componentDidMount = () => {
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide);
  }

  componentWillUnmount = () => {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();

    clearInterval(this.timer);
  }

  keyboardDidShow = () => {
    this.setState({ isShowKeyboard: true });
  }

  keyboardDidHide = () => {
    this.setState({ isShowKeyboard: false });
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      errors: nextProps.errors,
      isLoading: nextProps.auth.isLoading,
      isAuthenticated: nextProps.auth.isAuthenticated
    });
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.running !== prevState.running) {
      switch (this.state.running) {
        case true:
          this.handleStart();
      }
    }
  }

  // User log in
  onModal = () => {
    const { email, password, validPassword, errors } = this.state;
    const user = {
      email,
      password
    }

    this.setState({ isLoading: true });

    if (password === validPassword && Object.keys(errors).length === 0) {

      // Loading imitation
      setTimeout(() => {
        this.props.loginUser(user);
        this.setState({
          isModal: true,
          password: '',
          isAuthenticated: true,
          isLoading: false,
          isValidInput: true,
          disabled: 'disabled'
        });
      }, 1000);

    } else {

      // Loading imitation
      setTimeout(() => {
        this.setState({
          errors: { message: 'Введено не вірний VCODE. Спробуйте знову.' },
          isLoading: false,
          isValidInput: false,
          disabled: 'disabled'
        });
      }, 1000);

    }

    setTimeout(() => {
      this.clearErrors();
    }, 4000)
  }

  goBack = () => {
    this.props.navigation.goBack();
  }

  onCancel = () => {
    this.setState({ isModal: false });
  }

  // Input change handler
  changeHandler = (value) => {
    if (value.length > 3) {
      this.setState({ disabled: '' });
    } else {
      this.setState({ disabled: 'disabled' });
    }
    this.setState({ password: value });
  }

  // Countdown interval
  handleStart = () => {
    this.timer = setInterval(() => {
      const newCount = this.state.count - 1;
      this.setState(
        { count: newCount >= 0 ? newCount : 0 }
      );
      if (!this.state.count) {
        clearInterval(this.timer);
        this.setState({
          smsRequestDisabled: '',
          running: false,
          smsRequestText: 'Відправити ще раз'
        });
      }
    }, 1000);
  }

  // Countdown init
  handleCountdown = (seconds) => {
    this.setState({
      count: seconds,
      running: true
    })
  }

  smsRequest = () => {
    this.handleCountdown(10, 10);
    this.setState({ smsRequestDisabled: 'disabled' })
  }

  clearErrors = () => {
    this.setState({ errors: {} })
  }

  clearInput = () => {
    this.setState({ isValidInput: true, disabled: '', password: '' })
  }

  render() {

    const {
      isModal,
      isLoading,
      isAuthenticated,
      password,
      errors,
      disabled,
      isShowKeyboard,
      smsRequestDisabled,
      count,
      isValidInput,
      smsRequestText
    } = this.state;

    return (
      <ImageBackground source={BgImage} style={styles.wrapper}>
        <Modal
          title="Гаманець успішно з'єднано"
          content="Тепер ви можете швидко поповнити рахунок своєю банківськую картою собі або близьким та друзям!"
          visible={isAuthenticated && isModal}
          buttonText="Гаразд"
          buttonOnClick={this.onCancel}
        />

        <KeyboardAvoidingView
          style={styles.keyboardView}
          behavior="padding"
          enabled
        >

          {Object.keys(errors).length !== 0 &&
            <Notification message={errors} onSwipe={this.clearErrors} />
          }

          <Header title="Підключення Masterpass-гаманця" onClick={this.goBack} />

          <View style={styles.container}>
            <Text style={styles.text}>
              Щоб впевнитися що ви особисто підключаєте Masterpass-гаманець, ми
              тимчасово заблокуємо 1 гривню на картці із цього гаманця. Після
              цього вам надійде СМС з кодом підтвердження (VCODE) на той номер,
              який ви вказали у банку під час отримання картки.
            </Text>

            <Input
              placeholder="Введіть VCODE c SMS"
              onChangeText={this.changeHandler}
              onSubmitEditing={Keyboard.dismiss}
              value={password}
              error={isValidInput}
              onClose={this.clearInput}
            />

            {
              isLoading &&
              <ActivityIndicator
                color="#fdda24"
                animating={true}
                size="large"
              />
            }

            <View style={styles.feed}>
              <Button
                onClick={this.smsRequest}
                disabled={smsRequestDisabled}
                text={smsRequestText}
              />
              <Countdown time={count} />
            </View>
          </View>

          {isShowKeyboard &&
            <View style={styles.hiddenButton}>
              <Button
                onClick={this.onModal}
                type="fullWidth"
                disabled={disabled}
                text="Підключити"
              />
            </View>
          }

        </KeyboardAvoidingView >

        <Footer />

      </ImageBackground >
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1
  },
  keyboardView: {
    flex: 1
  },
  container: {
    paddingHorizontal: 20,
    flex: 1
  },
  text: {
    fontFamily: 'MuseoSansCyrl-500',
    color: '#fff',
    lineHeight: 19
  },
  hiddenButton: {
    marginTop: 'auto',
  },
  feed: {
    height: 45,
    marginTop: 20,
    flexDirection: 'row',
    alignItems: 'center'
  }
});

Step2.propTypes = {
  loginUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

export default connect(
  mapStateToProps,
  { loginUser }
)(Step2);
