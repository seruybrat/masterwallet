import React, { Component } from 'react';
import { StyleSheet, ImageBackground, Text, View, ActivityIndicator } from 'react-native';
import { connect } from "react-redux";
import PropTypes from "prop-types";

import Notification from "../../components/Notification/Notification";
import { registerUser } from "../../store/actions/authActions";
import Header from "../../components/Header/Header";
import Footer from "../../components/Footer/Footer";
import Button from "../../components/Button/Button";
import BgImage from "../../assets/images/bg.png";


class Step1 extends Component {

  // Reset header navigation
  static navigationOptions = {
    header: null
  };

  state = {
    isLoading: false,
    email: 'react@native.facebook.com',
    password: 'pistol',
    errors: {}
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      errors: nextProps.errors,
      isLoading: nextProps.auth.isLoading
    });
  }

  // User registration
  onConnect = () => {
    const { email, password } = this.state;
    const newUser = {
      email,
      password
    };

    // Loading imitation
    this.setState({ isLoading: true });

    setTimeout(() => {
      this.props.registerUser(newUser, this.props.navigation);
      this.setState({ isLoading: false });
    }, 1000);

  }

  goBack = () => {
    this.props.navigation.goBack();
  }

  clearErrors = () => {
    this.setState({ errors: {} })
  }

  render() {
    const { errors, isLoading } = this.state;

    return (
      <ImageBackground source={BgImage} style={styles.bgWrap}>

        <Header
          title="Підключення Masterpass-гаманця"
          onClick={this.goBack}
        />

        {Object.keys(errors).length !== 0 &&
          <Notification message={errors} onSwipe={this.clearErrors} />
        }

        <View style={styles.container}>
          <Text style={styles.text}>
            За номером
            <Text style={styles.textBold}> 067 220 56 18 </Text>
            вже є гаманець! Залишилось з’єднати його з додатком Мій
            Київстар, щоб поповнити рахунок.</Text>
        </View>

        {
          isLoading &&
          <ActivityIndicator
            color="#fdda24"
            animating={true}
            size="large"
          />
        }

        <View style={styles.buttonWrapper}>
          <Button onClick={() => { }} text="Пізніше" />
          <Button
            type="primary"
            onClick={this.onConnect}
            text="З'єднати"
          />
        </View>

        <Footer />
      </ImageBackground >
    );
  }
}

const styles = StyleSheet.create({
  bgWrap: {
    width: '100%',
    height: '100%'
  },
  container: {
    padding: 20
  },
  text: {
    fontFamily: 'MuseoSansCyrl-500',
    marginTop: 5,
    fontSize: 15,
    lineHeight: 19,
    color: '#fff'
  },
  textBold: {
    fontFamily: 'MuseoSansCyrl-700',
    fontWeight: 'bold'
  },
  buttonWrapper: {
    marginTop: 'auto',
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'center'
  },
});

Step1.propTypes = {
  registerUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object,
};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

export default connect(
  mapStateToProps,
  { registerUser }
)(Step1);

