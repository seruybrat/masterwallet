import {combineReducers} from 'redux';
import authReducer from './authReducer';
import errorReducer from './erroReducer';

const rootReducer = combineReducers({
    auth: authReducer,
    errors: errorReducer,
});

export default rootReducer;