import { SET_USER, GET_USER, LOADING } from "../actions/types";

const initialState = {
  isRegister: false,
  isAuthenticated: false,
  isLoading: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case SET_USER:
      return {
        ...state,
        isRegister: action.payload,
        loading: false
      };
    case GET_USER:
      return {
        ...state,
        isAuthenticated: action.payload,
        loading: false
      };
    case LOADING:
      return {
        ...state,
        loading: true
      };
    default:
      return state;
  }
}
