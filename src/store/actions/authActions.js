import axios from "axios";
import { GET_ERRORS, CLEAR_ERRORS, SET_USER, GET_USER, LOADING } from "./types";

// Register User
export const registerUser = (userData, history) => dispatch => {
  dispatch(setLoading());
  dispatch(clearErrors());
  axios
    .post("https://reqres.in/api/register", userData)
    .then(res => {
      dispatch({
        type: SET_USER,
        payload: true
      });
      history.navigate('Next');
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// Login Get User Token
export const loginUser = userData => dispatch => {
  dispatch(setLoading());
  dispatch(clearErrors());
  axios
    .post("https://reqres.in/api/login", userData)
    .then(res => {
      dispatch({
        type: GET_USER,
        payload: true
      })
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};


// Clear errors
export const clearErrors = () => {
  return {
    type: CLEAR_ERRORS
  }
}

//  loading
export const setLoading = () => {
  return {
    type: LOADING
  };
};