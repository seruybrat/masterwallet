import React from 'react';
import { StyleSheet, Text, View, Modal as NativeModal } from 'react-native';
import Button from "../Button/Button";

const Modal = (props) => {
  const { title, content, visible, buttonText, buttonOnClick } = props;

  return (

    <View style={visible && styles.wrap}>
      <NativeModal
        transparent={true}
        visible={visible}
        onRequestClose={() => { }}
        animationType={'slide'}
      >
        <View style={styles.modal}>
          <View style={styles.modalContent}>
            <Text style={styles.modalTitle}>{title}</Text>
            <Text style={styles.modalText}>{content}</Text>
            <Button onClick={buttonOnClick} type="primary" text={buttonText} />
          </View>
        </View>

      </ NativeModal>
    </View>
  )

}

const styles = StyleSheet.create({
  wrap: {
    backgroundColor: 'rgba(0,0,0,0.5)',
    position: 'absolute',
    flex: 1,
    width: '100%',
    height: '100%',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    zIndex: 3
  },
  modal: {
    padding: 20,
    flex: 1,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 4

  },
  modalContent: {
    backgroundColor: '#fff',
    borderRadius: 7,
    height: 198,
    paddingTop: 20,
    paddingBottom: 20,
    paddingLeft: 24,
    paddingRight: 24,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  modalTitle: {
    fontFamily: 'MuseoSansCyrl-900',
    textAlign: 'left',
    fontSize: 17,
    color: '#3B4952',
    marginBottom: 14
  },
  modalText: {
    fontFamily: 'MuseoSansCyrl-500',
    fontSize: 13,
    color: '#90979B',
    marginBottom: 25
  }
});

export default Modal;