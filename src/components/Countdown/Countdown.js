import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { MaterialIcons as Icon } from '@expo/vector-icons';


const Countdown = (props) => {
  const { time } = props;

  // Countdown formatting
  format = (time) => {
    let seconds = time % 60;
    let minutes = Math.floor(time / 60);
    minutes = minutes.toString().length === 1 ? "0" + minutes : minutes;
    seconds = seconds.toString().length === 1 ? "0" + seconds : seconds;
    return minutes + ':' + seconds;
  }

  return (
    <View style={styles.countdown}>
      <Text style={styles.countdownValue}>{format(time)}</Text>
      <Icon name="timer" style={styles.countdownIcon} />
    </View>
  )

}

const styles = StyleSheet.create({
  countdown: {
    height: 45,
    flexDirection: 'row',
    alignItems: 'center'
  },
  countdownValue: {
    width: 40,
    fontFamily: 'MuseoSansCyrl-500',
    fontSize: 13,
    color: '#fdda24'
  },
  countdownIcon: {
    marginLeft: 5,
    fontSize: 19,
    color: '#fdda24'
  }
});

export default Countdown;