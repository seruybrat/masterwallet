import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableWithoutFeedback, Animated, Easing } from 'react-native';
import { MaterialIcons as Icon } from '@expo/vector-icons';

class Button extends Component {

  componentWillMount = () => {
    this.animatedValue = new Animated.Value(1);
    this.animatedValue2 = new Animated.Value(0);
  }

  componentDidMount = () => {
    Animated.stagger(300, [
      Animated.timing(this.animatedValue2, {
        toValue: 45,
        duration: 1000,
        easing: Easing.bounce
      })
    ]).start();
  }

  handlePressIn = () => {
    Animated.spring(this.animatedValue, {
      toValue: .9
    }).start()
  }

  handlePressOut = () => {
    Animated.spring(this.animatedValue, {
      toValue: 1,
      friction: 3,
      tension: 40
    }).start()
  }

  render() {
    const { disabled, onClick, text, type } = this.props;

    const animatedStyle = {
      transform: [{ scale: this.animatedValue }]
    }

    const animatedStyle2 = {
      height: this.animatedValue2
    }
    return (

      <TouchableWithoutFeedback
        onPress={disabled !== 'disabled' ? onClick : () => { }}
        onPressIn={this.handlePressIn}
        onPressOut={this.handlePressOut}
      >
        {type === 'primary' ?

          <Animated.View style={disabled !== 'disabled' ? [styles.buttonPrimary, animatedStyle] : styles.buttonPrimaryDisabled}>
            <Text style={styles.textPrimary}>{text}</Text>
          </Animated.View> :

          type === 'fullWidth' ?

            <Animated.View style={disabled !== 'disabled' ? [styles.buttonFullWidth, animatedStyle2] : [styles.buttonFullWidthDisabled, animatedStyle2]}>
              <Text style={styles.textPrimary}>{text}</Text>
            </Animated.View> :

            <View style={styles.button}>
              <View style={styles.iconWrap}>
                <Icon style={styles.icon} name="chevron-right" />
              </View>
              <Text style={disabled !== 'disabled' ? styles.text : styles.textDisabled}>{text}</Text>
            </View>
        }

      </ TouchableWithoutFeedback>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 30,
    marginTop: -3,
    maxWidth: 200,
    height: 45,
    borderRadius: 23
  },
  buttonFullWidth: {
    width: '100%',
    backgroundColor: '#FDDA24',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonFullWidthDisabled: {
    width: '100%',
    height: 45,
    backgroundColor: '#ccc',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonPrimary: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 160,
    height: 45,
    backgroundColor: '#FDDA24',
    borderRadius: 23
  },
  textPrimary: {
    fontFamily: 'MuseoSansCyrl-700',
    fontSize: 16,
    color: '#3B4952'
  },
  text: {
    fontFamily: 'MuseoSansCyrl-700',
    fontSize: 16,
    color: '#ffffff'
  },
  textDisabled: {
    fontFamily: 'MuseoSansCyrl-700',
    fontSize: 16,
    color: '#ffffff',
    opacity: 0.5
  },
  iconWrap: {
    width: 14,
    height: 14,
    marginRight: 8,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FDDA24',
    borderRadius: 7,
  },
  icon: {
    fontSize: 10,
    color: '#3B4952'
  }
});

export default Button;