import React from 'react';
import { StyleSheet, TextInput, View, TouchableWithoutFeedback } from 'react-native';
import { MaterialIcons as Icon } from '@expo/vector-icons';

const Input = (props) => {
  const { placeholder, onChangeText, onSubmitEditing, value, error, onClose } = props;

  return (
    <View style={styles.wrap}>
      <TextInput
        style={error ? styles.inputText : styles.inputTextError}
        underlineColorAndroid='rgba(0,0,0,0)'
        placeholderTextColor='#ffffff'
        placeholder={placeholder}
        onChangeText={onChangeText}
        onSubmitEditing={onSubmitEditing}
        value={value}
      />
      {!error &&
        <TouchableWithoutFeedback onPress={onClose}>
          <Icon name="clear" style={styles.icon} />
        </TouchableWithoutFeedback >
      }
    </View>
  )

}

const styles = StyleSheet.create({
  wrap: {
    position: 'relative'
  },
  inputText: {
    fontFamily: 'MuseoSansCyrl-500',
    marginTop: 25,
    height: 40,
    borderColor: '#ffffff',
    borderBottomWidth: 1,
    color: '#ffffff',
    fontSize: 15
  },
  inputTextError: {
    fontFamily: 'MuseoSansCyrl-500',
    marginTop: 25,
    height: 40,
    borderColor: '#e2575b',
    borderBottomWidth: 1,
    color: '#ffffff',
    fontSize: 15
  },
  icon: {
    position: 'absolute',
    right: 0,
    bottom: 5,
    fontSize: 20,
    color: '#e2575b'
  }
});

export default Input;