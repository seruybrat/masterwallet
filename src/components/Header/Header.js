import React from 'react';
import { StyleSheet, Text, View, TouchableWithoutFeedback, Platform, StatusBar } from 'react-native';
import { MaterialIcons as Icon } from '@expo/vector-icons';

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 40 : StatusBar.currentHeight;

const Header = (props) => (

  <View style={styles.header}>
    <TouchableWithoutFeedback onPress={props.onClick}>
      <View style={styles.button}>
        <Icon style={styles.icon} name="arrow-back" />
      </View>
    </TouchableWithoutFeedback>
    <Text style={styles.title}>{props.title}</Text>
  </View>

)

const styles = StyleSheet.create({
  header: {
    padding: 20,
    marginTop: STATUSBAR_HEIGHT,
    backgroundColor: 'transparent',
    zIndex: 2
  },
  title: {
    fontFamily: 'MuseoSansCyrl-700',
    marginRight: 50,
    fontSize: 24,
    color: '#ffffff',
    lineHeight: 29
  },
  button: {
    marginBottom: 25
  },
  icon: {
    fontSize: 20,
    color: '#ffffff'
  }
});

export default Header;