import React from 'react';
import { StyleSheet, View } from 'react-native';
import Image from 'react-native-remote-svg';

const Footer = (props) => (

  <View style={styles.footer}>
    <Image
      style={styles.image}
      source={require('../../assets/images/master.svg')}
    />
  </View>

)

const styles = StyleSheet.create({
  footer: {
    marginTop: 60,
    padding: 20,
    alignItems: 'center'
  },
  image: {
    width: 176,
    height: 20
  }
});

export default Footer;