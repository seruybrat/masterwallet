import React, { Component } from 'react';
import { StyleSheet, View, Text, Animated, Dimensions, StatusBar, Platform } from 'react-native';
import GestureRecognizer, { swipeDirections } from 'react-native-swipe-gestures';
import Image from 'react-native-remote-svg';

const { width } = Dimensions.get("window");
const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 40 : StatusBar.currentHeight;

class Notification extends Component {

  componentWillMount = () => {
    this.animatedValue = new Animated.Value(0);
  }

  componentDidMount = () => {
    Animated.stagger(300, [
      Animated.timing(this.animatedValue, {
        toValue: width,
        duration: 100
      })
    ]).start();
  }

  render() {
    const { message, onSwipe } = this.props;
    const animatedStyle = {
      width: this.animatedValue
    }
    const config = {
      velocityThreshold: 0.3,
      directionalOffsetThreshold: 80
    };
    return (

      <GestureRecognizer
        onSwipeLeft={onSwipe}
        onSwipeRight={onSwipe}
        config={config}
        style={styles.notification}
      >
        {Object.keys(message).map((key) => {
          return <Animated.View style={[styles.notificationInner, animatedStyle]} key={key}>
            <Text style={styles.notificationText}>{message[key]}</Text>
          </Animated.View>
        })
        }
      </GestureRecognizer>
    )
  }
}

const styles = StyleSheet.create({
  notification: {
    position: 'absolute',
    alignItems: 'center',
    top: STATUSBAR_HEIGHT,
    right: 0,
    left: 0,
    zIndex: 3
  },
  notificationInner: {
    padding: 10,
    backgroundColor: '#E2575B'
  },
  notificationText: {
    fontFamily: 'MuseoSansCyrl-500',
    color: '#ffffff'
  }
});

export default Notification;