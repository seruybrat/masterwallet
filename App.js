import React, { Component } from 'react';
import { Provider } from "react-redux";
import store from "./src/store/store";
import { createStackNavigator } from 'react-navigation';
import { StyleSheet, Text, ImageBackground } from 'react-native';
import { Font, AppLoading } from "expo";

import Step1 from "./src/pages/Step1/Step1";
import Step2 from "./src/pages/Step2/Step2";
import BgImage from "./src/assets/images/bg.png";

// Navigation transition
const BottomTransition = (index, position, height) => {
  const sceneRange = [index - 1, index, index + 1];
  const outputHeight = [height, 0, 0];
  const transition = position.interpolate({
    inputRange: sceneRange,
    outputRange: outputHeight,
  });

  return {
    transform: [{ translateY: transition }]
  }
}

// Navigation config
const NavigationConfig = () => {
  return {
    screenInterpolator: (sceneProps) => {
      const position = sceneProps.position;
      const scene = sceneProps.scene;
      const index = scene.index;
      const height = sceneProps.layout.initHeight;

      return BottomTransition(index, position, height)
    }
  }
}

// Navigation init
const RootStack = createStackNavigator(
  {
    Home: Step1,
    Next: Step2,
  },
  {
    initialRouteName: 'Home',
    transitionConfig: NavigationConfig
  }
);

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: true
    };
  }

  // Import fonts
  async componentWillMount() {
    await Font.loadAsync({
      'MuseoSansCyrl-500': require("./assets/fonts/MuseoSansCyrl-500.ttf"),
      'MuseoSansCyrl-700': require("./assets/fonts/MuseoSansCyrl-700.ttf"),
      'MuseoSansCyrl-900': require("./assets/fonts/MuseoSansCyrl-900.ttf"),
    });
    this.setState({ loading: false });
  }


  render() {

    // If fonts not loaded
    if (this.state.loading) {
      return (
        <ImageBackground source={BgImage} style={styles.container}>
          <AppLoading />
        </ImageBackground >
      );
    }

    return (

      <Provider store={store}>
        <RootStack />
      </Provider>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    height: '100%'
  }

});

export default App;
